# Advanced Replay Example

This script will perform a replay analysis using a `bash` script.

```txt 
.
|-- readme.md     -> the file you are reading right now
|-- config.json   -> replay config file
|-- multi_ts.json -> addtional data: multiple scenarios
|-- out           -> Output directory. Results will be writen in this directory 
|-- project.am5p  -> AccessMod project archive to import 
|-- script_p.R    -> Main script that will be run inside the container
`-- script.sh     -> Launch script from as server with `docker` running

```

The entry point is `script.sh`. From this directory, on a `*nix` OS (linux, MacOS), simply run 

```sh 
$ ./script.sh 
```

What will happen : 

- Check for existing files and directory 
- Launch the `accessmod` container using the image set
- The main command will execute `Rscript` on file `script.R`
- From the R script, inside the Docker container, the environment will be loaded, variables set 
- The project will be imported. 
- Custom script run.
- layers/tables set in `config.json` will be written in the `./out` directory

